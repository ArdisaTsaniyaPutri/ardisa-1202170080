<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'Salam Melet :p', 
			'image' => 'wad.jpg', 
		]); 

		DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'Jangan gigit aku', 
			'image' => 'wadd.jpg', 
		]); 

		DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'Hi nama aku bercu(beruang lucu)', 
			'image' => 'waddd.jpg', 
		]); 

		DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'i love koceang', 
			'image' => 'wadddd.jpg', 
		]);

		DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'Salam Kedip ;)', 
			'image' => 'waddddd.jpg', 
		]); 

		DB::table('post')->insert([ 
			'user_id' => 1,
			'caption' => 'Hi nama aku stitch ungu', 
			'image' => 'wadddddd.jpg', 
		]); 
    }
}
