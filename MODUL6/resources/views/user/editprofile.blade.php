@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-1">
        <div class="col-md-9">
            <form method="POST" action="{{route('user.store')}}"  enctype="multipart/form-data">
                {{csrf_field()}}
                <h2>Edit Profile</h2>
                <p>Title</p>
                <input type="text" class="form-control" name="titleProfile" required value="{{$edit[0]->title}}"><br>
                <p>Discription</p>
                <input type="text" class="form-control" name="descriptionProfile" required value="{{$edit[0]->description}}"><br>
                <p>URL</p>
                <input type="text" class="form-control" name="urlProfile" required value="{{$edit[0]->url}}"><br>
                <p>Profile Image</p>
                <input type="file" class="form-control-file" name="imageProfile" accept="image/*">
                <br>
                <button type="submit" class="btn btn-primary">Save Profile</button>
            </form>
        </div>
    </div>  
</div>
@endsection
