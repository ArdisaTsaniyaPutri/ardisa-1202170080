@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       @foreach($posts as $post)
        <div class="col-md-8">
            <div class="card my-2">
                <div class="card-header">
                    <img src="{{asset($post->user->avatar)}}" width="50" height="50" style="border-radius: 50%"> {{$post->user->name}}
                </div>

                <div class="card-body">
                    <center>
                    <a href="{{ route('post.show', $post->id) }}"><img src="{{$post->image}}" width="75%"></a>
                    </center>
                </div>
                <div class="card-footer">
                    <a href="{{ url('comment/'.$post->id) }}">
                        <i class="fa fa-heart-o" style="color: black;"></i>
                    </a> 
                    <a href="#detail_komentar">
                        <i class="fa far fa-comment-o"></i>
                    </a><br>
                    <b>{{$post->likes}} Like</b><br>
                    <p><b>{{$post->user->email}}</b> {{$post->caption}}</p>
                    <p>
                        @foreach($post->kometarPosts as $komentar)
                            @foreach($users as $user)
                                @if($komentar->user_id == $user->id && $post->id == $komentar->post_id)
                                    <b>{{$user->email}}</b> {{$komentar->comment}}<br>
                                @endif
                            @endforeach
                        @endforeach
                    </p>
                    <form action="{{route('comment.add')}}" method="POST">  
                        @csrf
                        <input type="hidden" name="post_id" value="{{$post->id}}">
                        <div class="input-group">
                            <input type="text" class="form-control" name="komentar" id="detail_komentar" required placeholder="Add a comment..">
                            <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Send</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
