<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-light bg-warning">
		<a class="navbar-brand" href="home.php"><img src="EAD.png" width="200"></a>
	    <ul class="nav justify-content-end">
	    	<?php if (isset($_SESSION["username"])) {?>
	    		<li class="nav-item dropdown active">
		    	<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		        	<a class="dropdown-item" href="Profile.php">Profile</a>
		        	<a class="dropdown-item" href="Cart.php">Cart</a>
		          	<div class="dropdown-divider"></div>
		          	<a class="dropdown-item" href="Logout.php">Log Out</a>
		        </div>
	     	</li>
	    	<?php } else {?>
	    		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalLogin">Login</a>
	      		</li>
	      		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalRegister">Register</a>
	      		</li>
	    	<?php } ?>
	    </ul>
	</nav>
	<div class="card my-3 mx-auto bg-warning text-white" style="width: 90%;">
	  	<div class="card-body">
	    	<span class="align-middle">
	    		<h1>Hello Coders</h1>
	    		<p>Welcome to our store,  please a look for the product you might buy</p>
	    	</span>
	  	</div>
	</div>
	<div class="row mx-auto" style="width: 90%;">
  		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 23rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/web_fundamental_logo_030519090933.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 15rem;">
			    	<h5 class="card-title">Belajar Dasar Web</h5>
			    	<p class="card-text"><b>Rp.75.000,-</b></p>
			    	<p class="card-text">Website merupakan halaman situs sistem informasi yang dapat diakses secara cepat. Website ini didasari dari adanya perkembangan teknologi informasi dan komunikasi.</pre>
			  	</div>
			  	<div class="card-body">
			    	<a href="buyCart.php?product=Belajar+Dasar+Web&price=75000" class="card-link btn btn-warning" role="button" style="width: 20rem;">Buy</a>
			  	</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 23rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/java_fundamental_logo_080119141920.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 15rem;">
			    	<h5 class="card-title">Belajar Dasar Java</h5>
			    	<p class="card-text"><b>Rp.125.000,-</b></p>
			    	<p class="card-text">Java adalah bahasa pemrograman yang dapat dijalankan di berbagai komputer termasuk telepon genggam. Bahasa ini awalnya dibuat oleh James Gosling.</p>
			  	</div>
			  	<div class="card-body">
			    	<a href="buyCart.php?product=Belajar+Dasar+Java&price=125000" class="card-link btn btn-warning" role="button" style="width: 20rem;">Buy</a>
			  	</div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 23rem;">
			  	<img src="https://www.dicoding.com/images/original/academy/memulai_pemrograman_dengan_python_logo_090719134021.jpg" class="card-img-top" alt="...">
			  	<div class="card-body" style="height: 15rem;">
			    	<h5 class="card-title">Belajar Dasar Python</h5>
			    	<p class="card-text"><b>Rp.175.000,-</b></p>
			    	<p class="card-text">Python adalah bahasa pemrograman interpretatif multiguna dengan filosofi perancangan yang berfokus pada tingkat keterbacaan kode.</p>
			  	</div>
			  	<div class="card-body">
			    	<a href="buyCart.php?product=Belajar+Dasar+Python&price=175000" class="card-link btn btn-warning" role="button" style="width: 20rem;">Buy</a>
			  	</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<form method="post" action="Login.php">
	      			<div class="modal-header">
	        			<h5 class="modal-title" id="loginModalLabel">Login</h5>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          					<span aria-hidden="true">&times;</span>
        				</button>
	      			</div>
	      			<div class="modal-body">
			        	<div class="form-group">
			            	<label for="recipient-email" class="col-form-label">Email Address</label>
			            	<input type="email" class="form-control" name="lgnEmail" placeholder="Enter Email" required>
			          	</div>
			          	<div class="form-group">
							<label for="recipient-password" class="col-form-label">Password</label>
							<input type="password" class="form-control" name="lgnPassword" placeholder="Enter Password" required>
			          	</div>
	      			</div>
	      			<div class="modal-footer">
	        			<input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close">
	        			<input type="submit" class="btn btn-primary" value="Login" >
	      			</div>
	  	  		</form>
	    	</div>
	  	</div>
	</div>

	<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
	    	<div class="modal-content">
	      		<form method="post" action="Register.php">
	      			<div class="modal-header">
	        			<h5 class="modal-title" id="registerModalLabel">Register</h5>
	        			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          				<span aria-hidden="true">&times;</span>
	        			</button>
	      			</div>
	      			<div class="modal-body">
	          			<div class="form-group">
	            			<label for="recipient-email" class="col-form-label">Email Address</label>
	            			<input type="email" class="form-control" name="rgsEmail" placeholder="Enter Email" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="recipient-username" class="col-form-label">Username</label>
	            			<input type="text" class="form-control" name="rgsUsername" placeholder="Enter Username" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="recipient-password" class="col-form-label">Password</label>
	            			<input type="password" class="form-control" name="rgsPassword" placeholder="Password" required>
	          			</div>
	          			<div class="form-group">
	            			<label for="message-repassword" class="col-form-label">Confirm Password</label>
	            			<input type="password" class="form-control" name="rgsConPassword" placeholder="Confirm Password" required>
	          			</div>
	      			</div>
	      			<div class="modal-footer">
	        			<input type="button" class="btn btn-secondary" data-dismiss="modal" value="Close">
	        			<input type="submit" class="btn btn-primary" value="Register">
	      			</div>
	  	  		</form>
	    	</div>
	  	</div>
	</div>
	<footer class="footer">
		<div class="container">
	    	<div class="footer text-center py-3">© EAD Store</div>
      	</div>
    </footer>
</body>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>